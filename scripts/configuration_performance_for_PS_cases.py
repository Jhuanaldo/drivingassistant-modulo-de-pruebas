import matplotlib.pyplot as plt
import numpy as np
from os import listdir
from re import search
from csv import reader

max_ocurrencies = 0
nps_statuses = { '3.1', '3.2', '5', '6' }

def study_cases():
	cases = []
	for file in listdir("results/study"):
		if file.endswith(".csv"):
			cases.extend([file])
	return cases

def fill_dictionary(dictionary, key):
	if key in dictionary:
		dictionary[key] += 1
		global max_ocurrencies
		if max_ocurrencies < dictionary[key]:
			max_ocurrencies = dictionary[key]
	else:
		dictionary[key] = 1

def fill_y_axis(x, x_labels, solved_data, processing_data):
	solved_y_axis = []
	processing_y_axis = []
	for value in x_labels:
		if value in solved_data:
			solved_y_axis.extend([(solved_data[value])])
		if value in processing_data:
			processing_y_axis.extend([(processing_data[value])])

	for list_data in [solved_y_axis, processing_y_axis]:
		if len(list_data) < len(x):
			list_data.extend([0] * (len(x) - len(list_data)))
	return solved_y_axis, processing_y_axis

def main():
	solved_data = {}
	processing_data = {}
	for study_case in study_cases():
		with open('results/study/' + study_case, 'r') as csv_file:
			for row in reader(csv_file, delimiter=','):
				transition_to = search("-> (\d+(\.\d+)?)", row[1]).group(1)
				if transition_to in nps_statuses:
					continue
				threashold = search(r"\d+", row[2]).group()
				ratio = search(r"\d+", row[3]).group()
				key = '%s-%s'%(threashold, ratio)
				if row[4] == " SOLVED!":
					fill_dictionary(solved_data, key)
				elif row[4] != " ERROR!":
					fill_dictionary(processing_data, key)

		x = np.arange(len(set([*solved_data, *processing_data])))
		x_labels = sorted(list(set([*solved_data, *processing_data])))
		solved_y_axis, processing_y_axis = fill_y_axis(x, x_labels, solved_data, processing_data)

		width = 0.3

		solved_line = plt.bar(x - width, solved_y_axis, width=width, color='green', align='center', label="solved")
		processing_line = plt.bar(x, processing_y_axis, width=width, color='blue', align='center', label="processing")
		plt.xticks(x, x_labels, rotation=70)
		plt.title('Configuration performance for ' + study_case + ' case. Only solution transitions')
		plt.legend(handles=[solved_line, processing_line])
		plt.savefig('results/study/' + study_case + '_ps.png')

if __name__ == "__main__":
    main()
