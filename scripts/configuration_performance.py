import matplotlib.pyplot as plt
import numpy as np
from os import listdir
from re import search
from csv import reader

max_ocurrencies = 0

def study_cases():
	cases = []
	for file in listdir("results/study"):
		if file.endswith(".csv"):
			cases.extend([file])
	return cases


def fill_dictionary(dictionary, key):
	if key in dictionary:
		dictionary[key] += 1
		global max_ocurrencies
		if max_ocurrencies < dictionary[key]:
			max_ocurrencies = dictionary[key]
	else:
		dictionary[key] = 1

def fill_y_axis(x, x_labels, error_data, solved_data, processing_data):
	solved_y_axis = []
	error_y_axis = []
	processing_y_axis = []
	for value in x_labels:
		if value in solved_data:
			solved_y_axis.extend([(solved_data[value])])
		if value in error_data:
			error_y_axis.extend([(error_data[value])])
		if value in processing_data:
			processing_y_axis.extend([(processing_data[value])])

	for list_data in [solved_y_axis, error_y_axis, processing_y_axis]:
		if len(list_data) < len(x):
			list_data.extend([0] * (len(x) - len(list_data)))
	return solved_y_axis, error_y_axis, processing_y_axis

def main():
	error_data = {}
	solved_data = {}
	processing_data = {}
	for study_case in study_cases():
		with open('results/study/' + study_case, 'r') as csv_file:
			for row in reader(csv_file, delimiter=','):
				threashold = search(r"\d+", row[2]).group()
				ratio = search(r"\d+", row[3]).group()
				key = '%s-%s'%(threashold, ratio)
				if row[4] == " ERROR!":
					fill_dictionary(error_data, key)
				elif row[4] == " SOLVED!":
					fill_dictionary(solved_data, key)
				# else:
				# 	fill_dictionary(processing_data, key)

		x = np.arange(len(set([*error_data, *solved_data, *processing_data])))
		x_labels = sorted(list(set([ *error_data, *solved_data, *processing_data ])))
		solved_y_axis, error_y_axis, processing_y_axis =\
			fill_y_axis(x, x_labels, error_data, solved_data, processing_data)

		width = 0.3

		solved_line = plt.bar(x - width, solved_y_axis, width=width, color='green', align='center', label="solved")
		# processing_line = plt.bar(x, processing_y_axis, width=width, color='blue', align='center', label="processing")
		error_line = plt.bar(x, error_y_axis, width=width, color='red', align='center', label="error")
	
		plt.xticks(x, x_labels, rotation=70)
		plt.title('Configuration performance for ' + study_case + ' case')
		plt.legend(handles=[solved_line, error_line])
		plt.savefig('results/study/' + study_case + '.png')

if __name__ == "__main__":
    main()
