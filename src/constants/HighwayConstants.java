package constants;

import org.opencv.core.Point;

import java.awt.image.BufferedImage;

public final class HighwayConstants {
	public static double HORIZON;

	//define inner-highway triangle
	public static double INNER_DELIMITER_START_X_R;
	public static double INNER_DELIMITER_START_Y_R;
	public static double INNER_DELIMITER_END_X_R;
	public static double INNER_DELIMITER_END_Y_R;
	public static double INNER_DELIMITER_START_X_L;
	public static double INNER_DELIMITER_START_Y_L;
	public static double INNER_DELIMITER_END_X_L;
	public static double INNER_DELIMITER_END_Y_L;
	public static Point START_INTRA_NOISE_DELIMITER_R;
	public static Point END_INTRA_NOISE_DELIMITER_R;
	public static Point START_INTRA_NOISE_DELIMITER_L;
	public static Point END_INTRA_NOISE_DELIMITER_L;

	public static double HEIGH;
	public static double WIDTH;
	public static int UPPER_LIMIT = Integer.MAX_VALUE;
	public static double LINE_MATCHING_MINIMUM = 0.15;

	public static double PIVOT_X_R;
	public static double PIVOT_Y_R;
	public static double PIVOT_X_L;
	public static double PIVOT_Y_L;

	public static int DEFAULT_CANNY_LOWTHRESHOLD = 15;
	public static int DEFAULT_CANNY_RATIO = 3;

	public static int CANNY_LOWTHRESHOLD = DEFAULT_CANNY_LOWTHRESHOLD;
	public static int CANNY_RATIO = DEFAULT_CANNY_RATIO;

	public static int DEFAULT_HOUGH_THRESHOLD = 30;
	public static int DEFAULT_HOUGH_MINLINESIZE = 30;
	public static int DEFAULT_HOUGH_LINEGAP = 10;
	public static int DEFAULT_HOUGH_DEGREE = 90;

	public static int HOUGH_THRESHOLD = DEFAULT_HOUGH_THRESHOLD;
	public static int HOUGH_MINLINESIZE = DEFAULT_HOUGH_MINLINESIZE;
	public static int HOUGH_LINEGAP = DEFAULT_HOUGH_LINEGAP;
	public static int HOUGH_DEGREE = DEFAULT_HOUGH_DEGREE;

	public HighwayConstants(BufferedImage frame) {
		HighwayConstants.HORIZON = frame.getWidth() / 2 - frame.getWidth() / 14;

		INNER_DELIMITER_START_X_R = 0;
		INNER_DELIMITER_START_Y_R = frame.getHeight() * 0.33;
		INNER_DELIMITER_END_X_R = HighwayConstants.HORIZON;
		INNER_DELIMITER_END_Y_R = frame.getHeight() * 0.5;
		INNER_DELIMITER_START_X_L = HighwayConstants.HORIZON;
		INNER_DELIMITER_START_Y_L = frame.getHeight() * 0.5;
		INNER_DELIMITER_END_X_L = 0;
		INNER_DELIMITER_END_Y_L = frame.getHeight() - frame.getHeight() * 0.33;

		setSTART_INTRA_NOISE_DELIMITER_R(new Point(
			HighwayConstants.INNER_DELIMITER_START_X_R,
			HighwayConstants.INNER_DELIMITER_START_Y_R));
		setEND_INTRA_NOISE_DELIMITER_R(new Point(
			HighwayConstants.INNER_DELIMITER_END_X_R, 
			HighwayConstants.INNER_DELIMITER_END_Y_R));
		setSTART_INTRA_NOISE_DELIMITER_L(new Point(
			HighwayConstants.INNER_DELIMITER_START_X_L, 
			HighwayConstants.INNER_DELIMITER_START_Y_L));
		setEND_INTRA_NOISE_DELIMITER_L(new Point(
			HighwayConstants.INNER_DELIMITER_END_X_L, 
			HighwayConstants.INNER_DELIMITER_END_Y_L));

		HEIGH = frame.getHeight();
		WIDTH = frame.getWidth();

		PIVOT_X_R = HORIZON;
		PIVOT_X_L = HORIZON;
		PIVOT_Y_R = HEIGH * 0.5 + HEIGH * 0.04;
		PIVOT_Y_L = HEIGH * 0.5 - HEIGH * 0.04;
	}

	public static void setSTART_INTRA_NOISE_DELIMITER_R(
			Point startIntraNoiseDelimiterR) {
		START_INTRA_NOISE_DELIMITER_R = startIntraNoiseDelimiterR;
	}

	public static Point getEND_INTRA_NOISE_DELIMITER_R() {
		return END_INTRA_NOISE_DELIMITER_R;
	}

	public static void setEND_INTRA_NOISE_DELIMITER_R(
			Point endIntraNoiseDelimiterR) {
		END_INTRA_NOISE_DELIMITER_R = endIntraNoiseDelimiterR;
	}

	public static Point getSTART_INTRA_NOISE_DELIMITER_L() {
		return START_INTRA_NOISE_DELIMITER_L;
	}

	public static void setSTART_INTRA_NOISE_DELIMITER_L(
			Point startIntraNoiseDelimiterL) {
		START_INTRA_NOISE_DELIMITER_L = startIntraNoiseDelimiterL;
	}

	public static Point getEND_INTRA_NOISE_DELIMITER_L() {
		return END_INTRA_NOISE_DELIMITER_L;
	}

	public static void setEND_INTRA_NOISE_DELIMITER_L(
			Point endIntraNoiseDelimiterL) {
		END_INTRA_NOISE_DELIMITER_L = endIntraNoiseDelimiterL;
	}

	public static void setCANNY_LOWTHRESHOLD(int cannyLowthreshold) {
		CANNY_LOWTHRESHOLD = cannyLowthreshold;
	}

	public static void setCANNY_RATIO(int ccannyRatio) {
		CANNY_RATIO = ccannyRatio;
	}

	public static void setHOUGH_THRESHOLD(int choughThreshold) {
		HOUGH_THRESHOLD = choughThreshold;
	}

	public static void setHOUGH_MINLINESIZE(int choughMinlinesize) {
		HOUGH_MINLINESIZE = choughMinlinesize;
	}

	public static void setHOUGH_LINEGAP(int choughLinegap) {
		HOUGH_LINEGAP = choughLinegap;
	}

	public static void setHOUGH_DEGREE(int choughDegree) {
		HOUGH_DEGREE = choughDegree;
	}

	public static void set_to_default() {
		CANNY_LOWTHRESHOLD = DEFAULT_CANNY_LOWTHRESHOLD;
		CANNY_RATIO = DEFAULT_CANNY_RATIO;
		HOUGH_THRESHOLD = DEFAULT_HOUGH_THRESHOLD;
		HOUGH_MINLINESIZE = DEFAULT_HOUGH_MINLINESIZE;
		HOUGH_LINEGAP = DEFAULT_HOUGH_LINEGAP;
		HOUGH_DEGREE = DEFAULT_HOUGH_DEGREE;
	}

}
