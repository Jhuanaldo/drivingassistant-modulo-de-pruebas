package constants;

public final class DataStorageConstants {
	
	public static String SESSION_NAME = "study/";
	public static String RESULT_PATH = "results/";
	public static String STUDY_CASE_PATH = "study_cases/";
	public static String CSV_RESULT_PATH = RESULT_PATH + SESSION_NAME;
	public static String SOLUTION_IMAGES_PATH = "highway_sol/";
	public static String DEFAULT_CASE = "default";
	public static String RAINY_NIGHT_CASE = "rainy_night";
	public static String NIGHT_CASE = "night";

	public static int MAX_ERROR_ALLOWED = 5;

	public static String[] every_study_case() {
		return new String[] {DEFAULT_CASE, RAINY_NIGHT_CASE, NIGHT_CASE};
	}
}