package util;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;

import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import constants.DataStorageConstants;

public class ExecutionLog {

	private static boolean in_session = false;
	private static boolean overwrite = false;
	private static boolean trace = false;

	public static void write_result(String study_case, String string) {
		String file_name = DataStorageConstants.CSV_RESULT_PATH + study_case + ".csv";
		boolean append;
		if (in_session) append = true;
		else {
			in_session = true;
			append = !overwrite;
		}

		try(FileWriter fw = new FileWriter(file_name, append);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw))
		{
			out.println(string);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void new_session() {
		in_session = false;
	}

	public static void print(String print_this) {
		if (trace) { System.out.println(print_this); }
	}

	public static void print(Object[] printers, String flag) {
		if (trace) {
			for (int i = 0; i < printers.length; i++) {
				if (flag != "") System.out.print(flag + ": " + printers[i] + "\t");
				else System.out.print(printers[i] + "\t");
			}
			System.out.println();
		}
	}

	public static Mat draw(Mat imageMat, Point[] data, Scalar color) {
		Point start_l = data[0];
		Point end_l = data[1];
		Point start_r = data[2];
		Point end_r = data[3];
		if (start_l != null && end_l != null) Imgproc.line(imageMat, start_l, end_l, color, 3);
		if (start_r != null && end_r != null) Imgproc.line(imageMat, start_r, end_r, color, 3);
		return imageMat;
	}

	public static void save(Object i, BufferedImage image, String ext, String path) {
		try {
			BufferedImage bi = image;
			File outputfile = new File(path + i + ".jpg");
			ImageIO.write(bi, "jpg", outputfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void save_after(String i, BufferedImage image, String save_path) {
		image = Util.rotate_90_dg(Util.rotate_90_dg(Util.rotate_90_dg(image)));
		save(i, image, "png", DataStorageConstants.RESULT_PATH + save_path + "/");
	}

}
