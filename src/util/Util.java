package util;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import org.opencv.core.Mat;
import org.opencv.core.Point;

public class Util {

	public static BufferedImage mat_to_buffered_image(Mat matrix, BufferedImage bimg) {
		if (matrix != null) {
			int cols = matrix.cols();
			int rows = matrix.rows();
			int elemSize = (int) matrix.elemSize();
			byte[] data = new byte[cols * rows * elemSize];
			int type;
			matrix.get(0, 0, data);
			switch (matrix.channels()) {
			case 1:
				type = BufferedImage.TYPE_BYTE_GRAY;
				break;
			case 3:
				type = BufferedImage.TYPE_3BYTE_BGR;
				byte b;
				for (int i = 0; i < data.length; i = i + 3) {
					b = data[i];
					data[i] = data[i + 2];
					data[i + 2] = b;
				}
				break;
			default:
				return null;
			}

			if (bimg == null || bimg.getWidth() != cols
					|| bimg.getHeight() != rows || bimg.getType() != type) {
				bimg = new BufferedImage(cols, rows, type);
			}
			bimg.getRaster().setDataElements(0, 0, cols, rows, data);
		} else {
			bimg = null;
		}
		return bimg;
	}

	public static BufferedImage rotate_image(BufferedImage image, int degree,double height,
			double width) {
		AffineTransform tx = new AffineTransform();
		tx.rotate(Math.toRadians(degree), height, width);

		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
		image = op.filter(image, null);
		BufferedImage type_converted = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		type_converted.getGraphics().drawImage(image, 0, 0, null);
		return type_converted;
	}

	public static BufferedImage rotate_90_dg(BufferedImage bi) {
		BufferedImage biFlip;
		try{
			int width = bi.getWidth();
		    int height = bi.getHeight();
		    biFlip = new BufferedImage(height, width, bi.getType());
		    for(int i=0; i<width; i++)
		        for(int j=0; j<height; j++)
		            biFlip.setRGB(height-1-j, width-1-i, bi.getRGB(i, j));
		}
		catch(NullPointerException e) {
			ExecutionLog.print("Null");
			biFlip = null;
		}
	    return biFlip;
	}
	
	public static BufferedImage rotate270DX(BufferedImage bi) {
		return rotate_90_dg(Util.rotate_90_dg(Util.rotate_90_dg(bi)));
	}

	public static double diff(double firt_value, double second_value) {
		return Math.abs(firt_value - second_value);
	}

	public static double get_slope(Point a, Point b) {
		double x1 = a.x, x2 = b.x;
		double y1 = a.y, y2 = b.y;
		return get_slope(x1, y1, x2, y2);
	}

	public static double get_slope(double x1, double y1, double x2, double y2) {
		double m;
		if (x2 - x1 == 0 || y2 - y1 == 0) m = 0;
		else m = ( (y2 - y1) / (x2 - x1) );
		return m;
	}

}
