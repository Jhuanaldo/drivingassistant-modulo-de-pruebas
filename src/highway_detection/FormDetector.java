package highway_detection;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import constants.HighwayConstants;
import constants.DataStorageConstants;
import util.ExecutionLog;
import util.Util;

public class FormDetector {

	private static int i = 0;

	private double dest_y_l, dest_x_l, orig_y_l, orig_x_l;
	private double dest_y_r, dest_x_r, orig_y_r, orig_x_r;

	//define in process line points
	private static Point green_start_l;
	private static Point green_end_l;
	private static Point green_start_r;
	private static Point green_end_r;

	private static Point teal_start_l;
	private static Point teal_end_l;
	private static Point teal_start_r;
	private static Point teal_end_r;

	private Point avg_slope_line_start;
	private Point avg_slope_line_end;

	//m avg's
	private double m_l_avg;
	private double m_r_avg;
	private int counter_m_l;
	private int counter_m_r;

	//bad result indicator
	private static boolean calculate_left_side = true;
	private static boolean calculate_right_side = true;

	Point start, end;

	public Mat[] get_lines_matrix(BufferedImage frame, int threshold, int minLineSize,
			int lineGap, int degree) {
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		//imageIo to Mat
		byte[] data = ((DataBufferByte) frame.getRaster().getDataBuffer()).getData();
		Mat imageMat = new Mat(frame.getHeight(), frame.getWidth(), CvType.CV_8UC3);
		imageMat.put(0, 0, data);

		//do smth.
		Mat houghMat = new Mat(frame.getHeight(), frame.getWidth(),CvType.CV_8UC3);
		Imgproc.cvtColor(imageMat, houghMat, Imgproc.COLOR_RGB2GRAY);

		//canny algorithm
		Imgproc.Canny(houghMat, houghMat, HighwayConstants.CANNY_LOWTHRESHOLD,
			HighwayConstants.CANNY_LOWTHRESHOLD * HighwayConstants.CANNY_RATIO);

		//line probabilistic detection
		Mat lines = new Mat();
		Imgproc.HoughLinesP(houghMat, lines, 1, Math.PI / degree, threshold, minLineSize, lineGap);
		return new Mat[] { lines, imageMat };
	}

	public Point[] clean_trash(BufferedImage frame, Mat lines, Mat imageMat)
			throws InterruptedException {
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		double[] vec;
		double x1, x2, y1, y2, m;
		i++;

		dest_y_r = 0; dest_x_r = 0;
		orig_y_r = HighwayConstants.HEIGH; orig_x_r = HighwayConstants.UPPER_LIMIT;
		orig_y_l = 0; orig_x_l = HighwayConstants.UPPER_LIMIT;
		dest_y_l = HighwayConstants.HEIGH; dest_x_l = 0;

		//m avg's
		m_r_avg = 0; m_l_avg = 0;
		counter_m_r = 0; counter_m_l = 0;

		for (int x = 0; x < lines.rows(); x++) {
			vec = lines.get(x, 0);
			x1 = vec[0];
			x2 = vec[2];
			y1 = vec[1];
			y2 = vec[3];

			if (x2 - x1 == 0 || y2 - y1 == 0) continue;
			else m = ((y2 - y1) / (x2 - x1));

			start = new Point(x1, y1);
			end = new Point(x2, y2);

			if (x2 > HighwayConstants.HORIZON - HighwayConstants.HORIZON / 4
				|| x1 > HighwayConstants.HORIZON - HighwayConstants.HORIZON / 4
				|| does_line_cross_layers(frame, x1, y1, x2, y2))
				continue;

			if (is_left_line(m, x1, x2, y1, y2)) {
				if (HighwayConstants.INNER_DELIMITER_START_X_L < x1
					&& HighwayConstants.INNER_DELIMITER_START_X_L < x2
					&& HighwayConstants.INNER_DELIMITER_START_Y_L < y1
					&& HighwayConstants.INNER_DELIMITER_START_Y_L < y2
					&& HighwayConstants.INNER_DELIMITER_END_X_L > x1
					&& HighwayConstants.INNER_DELIMITER_END_X_L > x2
					&& HighwayConstants.INNER_DELIMITER_END_Y_L < y1
					&& HighwayConstants.INNER_DELIMITER_END_Y_L < y2
					|| y1 < HighwayConstants.HEIGH / 2) continue;
				if (!match_with_haze(x2, y2, x1, y1, 0)) continue;

				// Imgproc.line(imageMat, start, end, new Scalar(0, 0, 255), 3);
				// Imgproc.putText(imageMat, Double.toString(x1), start,
				// 	Core.FONT_HERSHEY_PLAIN, 0.7 ,new  Scalar(0,255,255));
				// Imgproc.putText(imageMat, Double.toString(x2), end,
				// 	Core.FONT_HERSHEY_PLAIN, 0.7 ,new  Scalar(0,255,255));
				
				m_l_avg += m;
				counter_m_l += 1;
				if (x1 < orig_x_l && y1 > orig_y_l) {
					orig_x_l = x1;
					orig_y_l = y1;
				}
				if (x2 > dest_x_l && y2 < dest_y_l) {
					dest_x_l = x2;
					dest_y_l = y2;
				}
			}
			else if (is_right_line(m, x1, x2, y1, y2)) {
				if (HighwayConstants.INNER_DELIMITER_START_X_R < x1
					&& HighwayConstants.INNER_DELIMITER_START_X_R < x2
					&& HighwayConstants.INNER_DELIMITER_START_Y_R < y1
					&& HighwayConstants.INNER_DELIMITER_START_Y_R < y2
					&& HighwayConstants.INNER_DELIMITER_END_X_R > x1
					&& HighwayConstants.INNER_DELIMITER_END_X_R > x2
					&& HighwayConstants.INNER_DELIMITER_END_Y_R > y1
					&& HighwayConstants.INNER_DELIMITER_END_Y_R > y2
					|| y2 > HighwayConstants.HEIGH / 2) continue;
				if (!match_with_haze(x1, y1, x2, y2, 1)) continue;

				// Imgproc.line(imageMat, start, end, new Scalar(255, 0, 0), 3);
				// Imgproc.putText(imageMat,Double.toString(x1), start,
				// 	Core.FONT_HERSHEY_PLAIN, 1.0 ,new  Scalar(0,255,255));
				// Imgproc.putText(imageMat, Double.toString(x2), end,
				// 	Core.FONT_HERSHEY_PLAIN, 0.7 ,new  Scalar(0,255,255));

				m_r_avg += m;
				counter_m_r += 1;
				if (x1 < orig_x_r && y1 < orig_y_r) {
					orig_x_r = x1;
					orig_y_r = y1;
				}
				if (x2 > dest_x_r && y2 > dest_y_r) {
					dest_x_r = x2;
					dest_y_r = y2;
				}
			}
		}

		calculate_definitive_lines(imageMat);

		// Imgproc.line(imageMat, HighwayConstants.START_INTRA_NOISE_DELIMITER_R, HighwayConstants.END_INTRA_NOISE_DELIMITER_R, new Scalar(100, 100, 0), 3);
		// Imgproc.line(imageMat, HighwayConstants.START_INTRA_NOISE_DELIMITER_L, HighwayConstants.END_INTRA_NOISE_DELIMITER_L, new Scalar(100, 100, 0), 3);
		// Imgproc.line(imageMat, new Point(HighwayConstants.PIVOT_X_R, HighwayConstants.PIVOT_Y_R),
		// 		new Point(HighwayConstants.PIVOT_X_L, HighwayConstants.PIVOT_Y_L), new Scalar(255, 0, 0), 3);
		// Mat printed_lines = ExecutionLog.draw(imageMat, new Point[] {green_start_r, green_end_r, green_start_l, green_end_l}, new Scalar(0, 255, 0));
		// printed_lines = ExecutionLog.draw(printed_lines, new Point[] {teal_start_r, teal_end_r, teal_start_l, teal_end_l}, new Scalar(255, 255, 0));
		// BufferedImage frame_plus_lines = Util.mat_to_buffered_image(printed_lines, null);
		// ExecutionLog.save_after(Integer.toString(i), frame_plus_lines, DataStorageConstants.SESSION_NAME);

		// if (!calculate_left_side || !calculate_right_side) {
		// 	ExecutionLog.print("calculate_left_side " + calculate_left_side + ", green_start_l: " + green_start_l.x + " green_end_l: " + green_end_l.x + " teal_start_l: " + teal_start_l.x + " teal_end_l: " + teal_end_l.x);
		// 	ExecutionLog.print("calculate_right_side " + calculate_right_side + ", green_start_r: " + green_start_r.x + " green_end_r: " + green_end_r.x + " teal_start_r: " + teal_start_r.x + " teal_end_r: " + teal_end_r.x);
		// }

		return new Point[] {
			green_start_r, green_end_r, teal_start_r, teal_end_r,
			green_start_l, green_end_l, teal_start_l, teal_end_l
		};
	}

	public static void save_side_for_next_ite(String side, Point[] lines) {
		if (side == "right_side") {
			calculate_right_side = false;
			green_start_r = lines[0];
			green_end_r = lines[1];
			teal_start_r = lines[2];
			teal_end_r = lines[3];
		} else{
			calculate_left_side = false;
			green_start_l = lines[4];
			green_end_l = lines[5];
			teal_start_l = lines[6];
			teal_end_l = lines[7];
		}
	}

	public static void set_lines_to_default() {
		green_start_l = null;
		green_end_l = null;
		green_start_r = null;
		green_end_r = null;
		teal_start_l = null;
		teal_end_l = null;
		teal_start_r = null;
		teal_end_r = null;
	}

	public static void set_size_calculation_to_default() {
		calculate_right_side = true;
		calculate_left_side = true;
	}

	private boolean is_left_line(double m, double x1, double x2, double y1, double y2) {
		return (calculate_left_side
			&& m < - 1
			&& m > - 4
			&& y2 > (HighwayConstants.HEIGH / 2)
			&& y1 > (HighwayConstants.HEIGH / 2)
			&& x1 > (HighwayConstants.WIDTH / 15));
	}

	private boolean is_right_line(double m, double x1, double x2, double y1, double y2) {
		return (calculate_right_side
			&& m > 1
			&& m < 4
			&& y2 < (HighwayConstants.HEIGH / 2)
			&& y1 < (HighwayConstants.HEIGH / 2));
	}

	private void calculate_definitive_lines(Mat imageMat) {
		if (orig_x_r != HighwayConstants.UPPER_LIMIT && calculate_right_side) {
			avg_slope_line_start = new Point(orig_x_r, orig_y_r);
			m_r_avg /= counter_m_r;
			avg_slope_line_end = new Point(HighwayConstants.HORIZON, (m_r_avg
				* HighwayConstants.HORIZON) - (m_r_avg * orig_x_r) + orig_y_r);
			teal_start_r = avg_slope_line_start;
			teal_end_r = avg_slope_line_end;

			if(orig_y_r != HighwayConstants.HEIGH && dest_x_r != 0 && dest_y_r != 0) {
				green_start_r = new Point(orig_x_r, orig_y_r);
				green_end_r = new Point(dest_x_r, dest_y_r);
			}
		}

		if (orig_x_l != HighwayConstants.UPPER_LIMIT && calculate_left_side) {
			avg_slope_line_start = new Point(orig_x_l, orig_y_l);
			m_l_avg /= counter_m_l;
			avg_slope_line_end = new Point(HighwayConstants.HORIZON, (m_l_avg
				* HighwayConstants.HORIZON) - (m_l_avg * orig_x_l) + orig_y_l);
			teal_start_l = avg_slope_line_start;
			teal_end_l = avg_slope_line_end;

			if(orig_y_l != 0 && dest_x_l != 0 && dest_y_l != HighwayConstants.HEIGH) {
				green_start_l = new Point(orig_x_l, orig_y_l);
				green_end_l = new Point(dest_x_l, dest_y_l);
			}
		}
	}

	private boolean match_with_haze(double x1, double y1, double x2, double y2, int side) {
		double pivot_x;
		double pivot_y;
		if (side == 0) {
			pivot_x = HighwayConstants.PIVOT_X_R;
			pivot_y = HighwayConstants.PIVOT_Y_R;
		}
		else{
			pivot_x = HighwayConstants.PIVOT_X_L;
			pivot_y = HighwayConstants.PIVOT_Y_L;
		}
		double m_haze = ((y2 - pivot_y) / (x2 - pivot_x));
		double m = Util.get_slope(x1, y1, x2, y2);
		return Util.diff(m_haze, m) < 1;
	}

	private boolean does_line_cross_layers(BufferedImage frame, double x1, double y1, double x2,
			double y2) {
		Color c_orig = new Color(frame.getRGB((int)x1, (int)y1));
		Color c_dest = new Color(frame.getRGB((int)x2, (int)y2));
		int[] colors_orig = { c_orig.getRed(), c_orig.getGreen(), c_orig.getBlue() };
		int[] colors_dest = { c_dest.getRed(), c_dest.getGreen(), c_dest.getBlue() };

		for (int i = 0; i < 3; i++) {
			if (Util.diff(colors_orig[i], colors_dest[i]) > 50)
				return true;
		}
		return false;
	}
}