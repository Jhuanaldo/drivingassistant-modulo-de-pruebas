package configuration;

import java.util.Scanner;

import constants.DataStorageConstants;
import util.ExecutionLog;


public class Main {
	private static boolean every_image = true;

	public static void main(String[] args) {
		String[] every_study_case = DataStorageConstants.every_study_case();
		for(int i = 0; i < every_study_case.length; i ++){
			ExecutionLog.new_session();
			parameters_finding(every_study_case[i]);
		}
	}

	private static void parameters_finding(String study_case) {
		
		LoadData images_load = new LoadData(study_case);

		if (every_image) {
			images_load.begin_finding(-1, -1, every_image);
		}
		else {
			@SuppressWarnings("resource")
			Scanner in = new Scanner(System.in);
			ExecutionLog.print("Init: ");
			int init = in.nextInt();
			ExecutionLog.print("End: ");
			int end= in.nextInt();
			images_load.begin_finding(init, end, every_image);
		}

		ExecutionLog.print("Finding finished");
	}
}