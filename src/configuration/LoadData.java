package configuration;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import constants.DataStorageConstants;
import constants.HighwayConstants;
import util.ExecutionLog;
import util.Util;

public class LoadData {
	private static String study_case_name;
	private static String study_case_path;

	public LoadData(String study_case) {
		study_case_name = study_case;
		study_case_path = DataStorageConstants.STUDY_CASE_PATH + study_case + "/";
	}

	public void begin_finding(int init, int end, boolean every_image) {
		String path = create_dir(DataStorageConstants.SESSION_NAME);

		File study_case_dir = new File(study_case_path);
		File[] study_case = study_case_dir.listFiles();

		if (every_image) {
			init = 0;
			end = study_case.length;
		}

		if (init >= 0 && init < study_case.length && end > init) {
			if(end > study_case.length ) end = study_case.length;
			ExecutionLog.print("init " + init + " end " + end
					+ " study_case.length " + study_case.length);
			run_autoconf(study_case_dir, study_case, init, end, path);
		} else {
			ExecutionLog.print("Wrong data entrance");
		}
	}

	private void run_autoconf(File study_case_dir, File[] study_case, int init, int end,
			String path) {
		try {
			new HighwayConstants(Util.rotate_90_dg(ImageIO.read(study_case[0])));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (int i = init; i < end; i++) {
			if (study_case[i].getName().toLowerCase().endsWith("jpg")) {
				BufferedImage applied_parameters;
				ExecutionLog.print("Next image + " + i);
				try {
					BufferedImage first = ImageIO.read(study_case[i]);
					applied_parameters = Autoconfig.detect_highway_data(study_case_name,
						Util.rotate_90_dg(first), i);
					applied_parameters = Util.rotate270DX(applied_parameters);
					ExecutionLog.save(i, applied_parameters, "png", DataStorageConstants.RESULT_PATH + path + "/");
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static String create_dir(String session_name) {
		File dir = new File(DataStorageConstants.RESULT_PATH + session_name);

		if (!dir.exists()) {
			ExecutionLog.print("creating directory: " + dir.getName());
			boolean result = false;

			try {
				dir.mkdir();
				ExecutionLog.print("absolutepath " + dir.getAbsolutePath());
				result = true;
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			if (result) {
				ExecutionLog.print("directory created");
			}
		}
		else {
			String[] entries = dir.list();
			for (String s : entries) {
				File current_file = new File(dir.getPath(), s);
				if (current_file.getName().endsWith("csv")) continue;
				current_file.delete();
			}
		}
		return dir.getName();
	}

}
