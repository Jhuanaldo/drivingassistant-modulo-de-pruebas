package configuration;

import java.awt.image.BufferedImage;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

import constants.DataStorageConstants;
import constants.HighwayConstants;
import highway_detection.FormDetector;
import util.ExecutionLog;
import util.Util;

public class Autoconfig {

	private static FormDetector form_detector = new FormDetector();
	private static int i = 0;
	private static int error_count = 0;
	
	private static String last_state = "0";
	private static int lowethreshold_new_value = 0;
	private static int canny_new_value = 0;


	public static BufferedImage detect_highway_data(String study_case, BufferedImage frame,
		int id) throws InterruptedException {
		BufferedImage frame_with_lines = null;
		Mat[] raw_data = form_detector.get_lines_matrix(frame, HighwayConstants.HOUGH_THRESHOLD,
			HighwayConstants.HOUGH_MINLINESIZE, HighwayConstants.HOUGH_LINEGAP,
			HighwayConstants.HOUGH_DEGREE);
		Mat lines = raw_data[0];
		Mat image_mat = raw_data[1];

		Point [] post_processed_data = form_detector.clean_trash(frame, lines, image_mat);
		String state = which_state(post_processed_data);
		if (state != "1" && error_count < DataStorageConstants.MAX_ERROR_ALLOWED) {
			error_count++;
			if (error_count > 0) ExecutionLog.print("ERROR COUNT " + error_count);
			switch(state) {
				case "2.1":
				case "4.1":
					FormDetector.save_side_for_next_ite("left_side", post_processed_data);
					generate_lowethreshold_new_value();
					generate_canny_new_value();
					HighwayConstants.setCANNY_LOWTHRESHOLD(lowethreshold_new_value);
					HighwayConstants.setCANNY_RATIO(canny_new_value);
					ExecutionLog.print("STATE " + state + " redo with lowthreshold "
						+ HighwayConstants.CANNY_LOWTHRESHOLD + " and ratio "
						+ HighwayConstants.CANNY_RATIO);
					break;
				case "2.2":
				case "4.2":
					FormDetector.save_side_for_next_ite("right_side", post_processed_data);
					generate_lowethreshold_new_value();
					generate_canny_new_value();
					HighwayConstants.setCANNY_LOWTHRESHOLD(lowethreshold_new_value);
					HighwayConstants.setCANNY_RATIO(canny_new_value);
					ExecutionLog.print("STATE " + state + " redo with lowthreshold " 
						+ HighwayConstants.CANNY_LOWTHRESHOLD + " and ratio " 
						+ HighwayConstants.CANNY_RATIO);
					break;
				case "3.1":
				case "3.2":
				case "5":
				case "6":
					generate_lowethreshold_new_value();
					generate_canny_new_value();
					HighwayConstants.setCANNY_LOWTHRESHOLD(lowethreshold_new_value);
					HighwayConstants.setCANNY_RATIO(canny_new_value);
					ExecutionLog.print("STATE " + state + " redo with lowthreshold " 
						+ HighwayConstants.CANNY_LOWTHRESHOLD + " and ratio " 
						+ HighwayConstants.CANNY_RATIO);
					break;
				case "0":
					ExecutionLog.print("Error");
					break;
			}

			ExecutionLog.write_result(study_case, id + ", " + last_state + " -> "
				+ state + ", " + HighwayConstants.CANNY_LOWTHRESHOLD
				+ ", " + HighwayConstants.CANNY_RATIO + ", PROCESSING");

			last_state = state;
			return detect_highway_data(study_case, frame, id);
		}
		else {
			if (state == "1") {
				ExecutionLog.write_result(study_case, id + ", " + last_state
					+ " -> 1, " + HighwayConstants.CANNY_LOWTHRESHOLD
					+ ", " + HighwayConstants.CANNY_RATIO + ", SOLVED");
			}
			else {
				ExecutionLog.write_result(study_case, id + ", " + last_state + " -> "
					+ state + ", " + HighwayConstants.CANNY_LOWTHRESHOLD
					+ ", " + HighwayConstants.CANNY_RATIO + ", ERROR");
			}

			Mat printed_lines = 
				ExecutionLog.draw(image_mat, new Point[]{ post_processed_data[0], post_processed_data[1],
					post_processed_data[4], post_processed_data[5] },
					new Scalar(0, 255, 0));
			printed_lines = 
				ExecutionLog.draw(printed_lines, new Point[]{ post_processed_data[2], 
					post_processed_data[3], post_processed_data[6], post_processed_data[7] },
					new Scalar(255, 255, 0));

			frame_with_lines = Util.mat_to_buffered_image(printed_lines, null);
			image_mat.release();
			error_count = 0;
			i++;
			last_state = "0";
			HighwayConstants.set_to_default();
			FormDetector.set_lines_to_default();
			FormDetector.set_size_calculation_to_default();
			return frame_with_lines;
		}
	}

	private static String which_state(Point[] definitive_lines) {
		String left_side_case = "NOT_EXISTING";
		String right_side_case = "NOT_EXISTING";

		Point green_start_r = definitive_lines[0];
		Point green_end_r = definitive_lines[1];
		Point teal_start_r = definitive_lines[2];
		Point teal_end_r = definitive_lines[3];
		Point green_start_l = definitive_lines[4];
		Point green_end_l = definitive_lines[5];
		Point teal_start_l = definitive_lines[6];
		Point teal_end_l = definitive_lines[7];

		if (green_start_r != null && green_end_r != null) {
			right_side_case = "NOT_MATCH";
			if (Util.diff(
					Util.get_slope(green_start_r, green_end_r),
					Util.get_slope(teal_start_r, teal_end_r))
					< HighwayConstants.LINE_MATCHING_MINIMUM) {
				right_side_case = "MATCH";
			}
		}
		if (green_start_l != null && green_end_l != null) {
			left_side_case = "NOT_MATCH";
			if (Util.diff(
					Util.get_slope(green_start_l, green_end_l),
					Util.get_slope(teal_start_l, teal_end_l))
					< HighwayConstants.LINE_MATCHING_MINIMUM) {
				left_side_case = "MATCH";
			}
		}
		ExecutionLog.print("\t\t\t\t"+left_side_case + " " + right_side_case);
		return state(right_side_case, left_side_case);
	}

	private static String state(String right_side_case, String left_side_case) {
		String state = "0";
		if (left_side_case == "MATCH" && right_side_case == "MATCH") {
			state = "1";
		} else if (left_side_case == "MATCH" && right_side_case == "NOT_MATCH") {
			state = "2.1";
		} else if (left_side_case == "NOT_MATCH" && right_side_case == "MATCH") {
			state = "2.2";
		} else if (left_side_case == "NOT_MATCH" && right_side_case == "NOT_EXISTING") {
			state = "3.1";
		} else if (left_side_case == "NOT_EXISTING" && right_side_case == "NOT_MATCH") {
			state = "3.2";
		} else if (left_side_case == "MATCH" && right_side_case == "NOT_EXISTING") {
			state = "4.1";
		} else if (left_side_case == "NOT_EXISTING" && right_side_case == "MATCH") {
			state = "4.2";
		} else if (left_side_case == "NOT_EXISTING" && right_side_case == "NOT_EXISTING") {
			state = "5";
		} else if (left_side_case == "NOT_MATCH" && right_side_case == "NOT_MATCH") {
			state = "6";
		}
		return state;
	}

	private static void generate_lowethreshold_new_value() {
		lowethreshold_new_value = ((int) (Math.random() * 10) + 10);
	}

	private static void generate_canny_new_value() {
		canny_new_value = ((int) (Math.random() * 3) + 1);
	}
}
