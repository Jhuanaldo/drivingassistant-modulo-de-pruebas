n ?= 50

all: clean start_study stats

start_study:
	n=$(n); \
	while [ $${n} -gt 0 ] ; do \
	$(MAKE) run; \
	n=`expr $$n - 1`; \
	done; \
	true

run:
	javac -cp "lib/opencv-3.4.4/build/bin/opencv-344.jar" src/*/*.java -d bin/
	java -cp .:lib/opencv-3.4.4/build/bin/opencv-344.jar:bin configuration.Main

stats: config_performance config_performance_ps

config_performance:
	python3 scripts/configuration_performance.py

config_performance_ps:
	python3 scripts/configuration_performance_for_PS_cases.py

delete_results:
	rm -f results/study/*.jpg results/study/*.png

clean:
	rm -f results/study/*
